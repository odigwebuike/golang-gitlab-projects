module gitlab.com/odigwebuike/golang-gitlab-projects

go 1.20

require (
	github.com/aws/aws-lambda-go v1.40.0
	github.com/aws/aws-sdk-go v1.44.252
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
)

require (
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
)
