# Golang Bookstore Management

This project is a website for managing a bookstore. It is built using the Go programming language and various other open-source libraries.

## Features

The website has the following features:

- Adding, editing, and deleting books managed in a bookstore using simple CRUD functions

## Technologies Used

The project is built using the following technologies:

- Go programming language (v1.16)
- Gitlab for automated CI/CD

## Getting Started

To run the project locally, follow these steps:

1. Clone the repository: `git clone https://gitlab.com/odigwebuike/golang-gitlab-projects/-/tree/main/bookstore`
2. Install Go (v1.20 or later): https://golang.org/doc/install
3. Install project dependencies: `go mod download`
4. Run the project: `go run main.go`

## Contributing

Contributions to the project are welcome! If you'd like to contribute, please follow these steps:

1. Fork the repository
2. Create a new branch: `git checkout -b my-new-feature`
3. Make your changes and commit them: `git commit -am 'Add some feature'`
4. Push your changes to your fork: `git push origin my-new-feature`
5. Create a pull request

## Licence

This project is licensed under the MIT License. See the `Licence.md` file for details.
