package main

import (
	"github.com/gorilla/mux"
	"gitlab.com/odigwebuike/golang-gitlab-projects/bookstore/pkg/routes"
	"log"
	"net/http"
)

func main() {
	r := mux.NewRouter()
	routes.RegisterBookstoreRoutes(r)
	http.Handle("/", r)
	log.Fatal(http.ListenAndServe("localhost:9010", r))

}
